//Belajar Pop-Up
// var test = confirm ("Apakah kamu yakin");
// if (test === true ){
    // alert ('User menekan OKE');
// } else {
    // alert('User menekan Tidak');
// }

//Contoh apps Pop-up sederhana
// alert('Selamat Datang');
// var lagi = true;

// while (lagi === true){
//     var nama = prompt('Masukkan Nama:');
//     alert('Hello' + nama);

//     lagi = confirm('Coba lagi ?')
// }

// alert('Terima Kasih');

// Materi: Control Flow : mengekseskusi dari atas ke bawah
// Namun bisa di buat tidak berurutan dangn Pengulangan dan Pengondisian
// Pada perulangan ada for, while, do while
// Pada pengkondisian/percabangan yaitu : If , if else, if else if else, & switch

// ex pengulangan :
// alert('Mulai');
// for (var i = 0; i < 5; i++){
//     alert('Hello  World')
// // 
// //     alert('Selesai');

// // // ex : pendkondisian
// // var angka = prompt ('Masukkan Angka');
// // if (angka % 2 === 0){
// //     alert (angka + ' adalah bilangan GENAP');
// // } else if (angka % 2 === 1){ 
// //     alert(angka + ' adalah bilangan GANJIL');
// // }else{
// //     alert ('Bukan angka, silahkan masukkan angka');
// // }


// //*Belajar Pengulangan dengan WHILe*
// // RUMUS: selama nilai kondisi sesuai,maka aksi dalam blog dilakukan terus menrus
// // while (kondisi){
// //     aksi
// // }

// cara menghentikannya adalah ada 2,
// dihentikan user atau menggunakan program;

// // // ex : dihentikan oleh user
// // var ulang = true;
// // while (ulang){
// //     console.log('Hello Ghi');
// //     ulang = confirm ('Lagi ?');
// // }

// while dihentikan dengan program
// // dengan rumus 
// // nilai awal
// // while (kondisi terminasi){
// //     aksi

// //     increment / decrement
// // }
//  ex dihentikan oleh program
// // var nilaiAwal = 1;
// // while(nilaiAwal <=10){
// //     console.log('Heloo World' + nilaiAwal + 'x');

// //     nilaiAwal++;
// // }

//  pengulangan dengan menggunakan FOR
// // rumus
// // for (var nilaiAlai = 1; nilaiAwal <= 10; nilaiAwal++){
// //     console.log('Hello World');
// // }

// // for( var nilaiAwal = 1; nilaiAwal<= 10; nilaiAwal++){
// //     console.log("Hello World" + nilaiAwal + ' X')
// // }

//  *Belajar Pengkondisian IF*
// Rumus :
// if (kondisi){
//     aksi //lakukan jika kondisi bernilai True
// }


// Belajar Switch
// //Rumus
// switch(ekspresi){
//     case "nilai 1":
//     aksi 1
//     [break;]
//     case "nilai 2":
//     aksi 1
//     [break;]
//     case "nilai n":
//     aksi 1
//     [break;]
//     default;
//     aksi default
//     [break;]
// }

// ex kalau menggunakan if:

// var angka = prompt('masukkan angka: ');
// if (angka == 1 ){
//     alert("Anda memasukkan angka 1");
//     } else if (angka == 2){
//         alert("Anda memasukkan angka 2");
//     }else if (angka == 3){
//         alert("Anda memasukkan angka 3");
//     }else {
//         alert("Angka yang anda masukkan salah");
//     }

