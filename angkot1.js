// *Latihan Loop while dengan For

// var jmlAngkot = 10;
// var noAngkot = 1;
// var angkotBeroperasi = 8;

// while (noAngkot <= angkotBeroperasi ){
//     console.log('Angkot No. ' + noAngkot + " beroperasi dengan baik.");

// noAngkot++;
// }

// for(noAngkot = angkotBeroperasi + 1; noAngkot<= jmlAngkot; noAngkot++){
//     console.log("Angkot No. " + noAngkot + ' sedang bermasalah')
// }

// // Latihan angkot 3
// Kondisinya aadlah 1-6 baik, 1-10 tidak baik, dengan FOR saja
// var jmlAngkot = 10;
// var angkotBeroperasi = 6;

// for (var noAngkot = 1; noAngkot <= jmlAngkot; noAngkot++){
    
//     if (noAngkot<=6){
//         console.log ('Angkot No. ' + noAngkot + " berfungsi dengan baik");
//     } else {
//         console.log ('Angkot No. ' + noAngkot + " tidak berfungsi dengan baik");
//     }
    
// }

// Latihan Angkot 4 dgn kondisi 1-6 baik, 7 & 9-10 tdak, 8 lembur

// var jmlAngkot = 10;
// var angkotBeroperasi = 6;

// for (var noAngkot = 1; noAngkot <= jmlAngkot; noAngkot++){
    
//     if (noAngkot <= 6){
//         console.log ('Angkot No. ' + noAngkot + " berfungsi dengan baik");
//     } else if (noAngkot === 8){
//     console.log ('Angkot No. ' + noAngkot + " Sedang lembur");
//     } else{
//         console.log ('Angkot No. ' + noAngkot + " tidak berfungsi dengan baik");
//     }
// }

// // Latihan Angkot 5 dgn kondisi 1-6 baik, 7 & 9-10 tdak, 8 & 10 lembur

// var jmlAngkot = 10;
// var angkotBeroperasi = 6;

// for (var noAngkot = 1; noAngkot <= jmlAngkot; noAngkot++){
    
//     if (noAngkot <= 6){
//         console.log ('Angkot No. ' + noAngkot + " berfungsi dengan baik");
//     } else if (noAngkot === 8 || noAngkot ==10){
//     console.log ('Angkot No. ' + noAngkot + " Sedang lembur");
//     } else{
//         console.log ('Angkot No. ' + noAngkot + " tidak berfungsi dengan baik");
//     }
// }

// Latihan Angkot 6 dgn kondisi 1-6 baik selain 5, 7 & 9 tdak, 5,8 & 10 lembur

// var jmlAngkot = 10;
// var angkotBeroperasi = 6;

// for (var noAngkot = 1; noAngkot <= jmlAngkot; noAngkot++){
    
//     if (noAngkot <= 6 && noAngkot !==5){
//         console.log ('Angkot No. ' + noAngkot + " berfungsi dengan baik");
//     } else if (noAngkot === 5 || noAngkot === 8 || noAngkot ===10){
//     console.log ('Angkot No. ' + noAngkot + " Sedang lembur");
//     } else{
//         console.log ('Angkot No. ' + noAngkot + " tidak berfungsi dengan baik");
//     }
// }